#include<stdlib.h>
#include<stdio.h>
#include<sys/ptrace.h>

int main()
{
	// ask ptrace to trace this process
	if (ptrace(PTRACE_TRACEME, 0, 0, 0) < 0) {
		// error: the process is already being traced!
		puts("I don't like debuggers. Bye!");
		return 1;
	}
	puts("Glad to see you're not using a debugger :)");
	return 0;
}

