# vim: set maxmempattern=2048:

.att_syntax
.global _start

.text

# GAME FORMAT

# Input must be 0 to 256 bytes, encoded into 0 to 512 bytes of (uppercase) hex.
# first 8 bytes must match the file signature "NIIv1.0:"
# next 8 bytes must match a valid game title (e.g. "AmnlXing")
# next 4 bytes are a CRC-32 of the code portion of the game
# remaining bytes are game code

error:
	movq $60, %rax
	movq $1, %rdi
	syscall

decode_hexchar:
	cmpb $0x30, %al
	jl error
	cmpb $0x39, %al
	jle dh_digit
	cmpb $0x41, %al
	jl error
	cmpb $0x46, %al
	jg error
	subb $0x07, %al
	dh_digit:
		subb $0x30, %al
		retq

_start:

	movq %rsp, %rbp
#	subq $X, %rsp

# WELCOME
	movq $1, %rax	# write
	movq $1, %rdi
	leaq s_welcome, %rsi
	movq $0x59, %rdx
	syscall

# CREATE_MEMORY_RW
	movq $9, %rax	# mmap
	xorq %rdi, %rdi		# addr
	movq $0x1000, %rsi	# len (page size)
	movq $0x1, %rdx		# prot (+r
	orq $0x2, %rdx		# +w)
	movq $0x2, %r10		# flags (private
	orq $0x20, %r10		# anonymous)
	movq $-1, %r8		# fd
	xorq %r9, %r9		# off
	syscall
	# remove when ready:
	movq %rax, p_game
	movb $0xC3, (%rax)	# ret

# INPUT
	xorq %rax, %rax	# read
	xorq %rdi, %rdi		# fd
	leaq game_enc, %rsi	# buf
	movq $0x200, %rdx	# count
	syscall
	movq %rax, %rbx	# nbytes read
	shrq $1, %rbx
	movw %bx, game_len
	xorq %rcx, %rcx
	decode_hexpair:
		movb 0(%rsi, %rcx, 2), %al
		callq decode_hexchar
		salw $12, %ax	# %al -> %ah; salb $4, %ah
		movb 1(%rsi, %rcx, 2), %al
		callq decode_hexchar
		addb %ah, %al
		movb %al, 0(%rsi, %rcx)
		incq %rcx
		cmpq %rbx, %rcx
		jl decode_hexpair

# SCRAMBLE (TODO)
	# check for valid 8-byte signature
	movq $0x3A312E307649494E, %rax	# "NIIv1.0:"
	cmpq %rax, (%rsi)
	jne error
	# check for valid 8-byte title
	movq $0x636E7250746C7754, %rax	# "TwltPrnc"
	cmpq %rax, 8(%rsi)
	je header_ok
	movq $0x747261436F72614D, %rax	# "MaroCart"
	cmpq %rax, 8(%rsi)
	je header_ok
	movq $0x2B2B73656E746946, %rax	# "Fitnes++"
	cmpq %rax, 8(%rsi)
	je header_ok
	movq $0x676E69586C6E6D41, %rax	# "AmnlXing"
	cmpq %rax, 8(%rsi)
	je header_ok
	jmp error
	header_ok:
	# validate CRC
	xorq %rbx, %rbx
	xorq %rdx, %rdx
	movl 0x10(%rsi), %edx
	leaq 0x14(%rsi), %rsi
	movzwq game_len, %rbx
	# apped zeroes
	movl $0, 0x14(%rsi, %rbx)
	# polynomial: x³²+x³¹+x⁴+1
	movq $0b110000000000000000000000000010001, %rax
	# %rax: polynomial; %bl: message byte; %r8: loop counter
	# %edi: remainder; %edx: checksum from game; %rsi: address of byte
	movq $0x14, %r8
	crc_byte:
		movb (%rsi), %bl
		movb $7, %cl
		crc_bit:
			xorq %r9, %r9
			# shift remainder accumulator
			shll $1, %edi
			# check if bit shifted out was 1
			cmovcq %rax, %r9
			# move next bit of message byte into remainder's LSB
			movb %bl, %r10b
			shrb %cl, %r10b
			andb $1, %r10b
			xorb %r10b, %dil
			# remainder ^= poly iff bit shifted out was 1
			xorq %r9, %rdi
			# loop
			decb %cl
			cmpb $0, %cl
			jge crc_bit
		incq %rsi
		incq %r8
		cmpw game_len, %r8w
		jl crc_byte
	cmpl %edi, %edx
	jne error

# COPY
	leaq game_enc, %rsi
	leaq 0x14(%rsi), %rsi
	movq p_game, %rdi
	movzwq game_len, %rcx
	subq $0x14, %rcx
	rep movsb (%rsi), (%rdi)

# GAMING
	movq $10, %rax	# mprotect
	movq p_game, %rdi	# addr (p_game)
	movq $0x1000, %rsi	# len (page size)
	movq $0x1, %rdx		# prot (+r
	orq $0x4, %rdx		# +x)
	syscall
	movq p_game, %rax
	callq *%rax

exit:
	movq $60, %rax
	movq $0, %rdi
	syscall
	.ascii "int main(){puts(flag);}"	# lol

.data
p_game: .quad 0x03010102464C457F	# ELF header ;)
s_welcome:
	.ascii "\xE4\xBA\x8C\xE3\x80\x87\xE4\xBA"
	.ascii "\x8C\xE3\x80\x87\xE5\xB9\xB4\xEF"
	.ascii "\xBC\x8C\xE7\xA8\xB3\xE5\xA4\xA9"
	.ascii "\xE5\xA0\x82\xE8\xBD\xAF\xE4\xBB"
	.ascii "\xB6\xE5\x85\xAC\xE5\x8F\xB8\xE2"
	.ascii "\x80\x94\xE2\x80\x94\xE7\x89\x88"
	.ascii "\xE6\x9D\x83\xE6\x89\x80\xE6\x9C"
	.ascii "\x89\xE3\x80\x82\x0A\xE8\xAF\xB7"
	.ascii "\xE6\x8F\x92\xE5\x85\xA5\xE6\xB8"
	.ascii "\xB8\xE6\x88\x8F\xE7\xA3\x81\xE7"
	.ascii "\x9B\x98\xE2\x8B\xAF\xE2\x8B\xAF"
	.ascii "\x0A\x00"
game_len: .short 0
game_enc:
	.long 0x90C03148
	.word 0x050F	# all just junk, will be overwritten
	.fill 0x200-(.-game_enc), 1, 0x90
	.long 0	# extra space for appended CRC width

