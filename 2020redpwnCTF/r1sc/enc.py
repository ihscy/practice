#!/usr/bin/env python3

flag = "flag{actually_3_instructions:_subleq,_ret,_int3}"

assert len(flag) == 6*8

a = 0x00D9CD4AB6A2D747
b = 0x016069317E428CA9
d = 0

for i in range(0, 6*8, 8):
    f = int.from_bytes(flag[i:i+8].encode("ascii"), "little")
    k = a - 1
    print(int.to_bytes(f-k, 8, "little").hex(r'_').upper())
    d = a + b
    a = b
    b = d

