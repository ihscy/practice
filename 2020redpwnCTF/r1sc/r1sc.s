.att_syntax
.global _start

.text

_start:

	leaq s_enter(%rip), %rsi
	callq puts

	xorq %rdi, %rdi
	leaq t_chk(%rip), %rsi
	movq $0x30, %rdx
	# sys_read
	xorq %rax, %rax
	syscall

	leaq tbl(%rip), %rbp
	xorq %rsi, %rsi
	callq subleq

	cmpq $0, tbl(%rip)	# return code, this time
	je win
	leaq s_lose(%rip), %rsi
	callq puts
	movq $-1, %rdi
	jmp exit

	win:
		leaq s_win(%rip), %rsi
		callq puts
		xorq %rdi, %rdi
	exit:
		# sys_exit
		movq $0x3C, %rax
		syscall

puts:
	movq $1, %rdi
	movzbq -1(%rsi), %rdx
	# sys_write
	movq $1, %rax
	syscall
	retq

subleq:
	# branch to index 1 to quit
	cmpq $1, %rsi
	jne subcheck
	retq
	subcheck:
		movq (%rbp, %rsi, 8), %rax
		movq (%rbp, %rax, 8), %rax
		movq 8(%rbp, %rsi, 8), %rbx
		subq %rax, (%rbp, %rbx, 8)
		ja next
		cmpq $2, 0x10(%rbp, %rsi, 8)
		je dbp
		cmpq $0, 0x10(%rbp, %rsi, 8)
		je next
		movq 0x10(%rbp, %rsi, 8), %rsi	# branch
		jmp subleq
	dbp:
		int3	# let's hope some people find this :)
	next:
		addq $3, %rsi
		jmp subleq


.data

.byte 19
s_enter: .ascii "Enter access code: "
.byte 19
s_win: .ascii "Access authorized.\n"
.byte 15
s_lose: .ascii "Access denied.\n"

.macro SUBLEQ a=9, b=9, c=0
	# default to SUBLEQ Z, Z
	.quad \a; .quad \b; .quad \c
.endm

.macro _DBP
	.quad 9; .quad 9; .quad 2
.endm

.macro _ADD a=9, b=9
	SUBLEQ \a, 9
	SUBLEQ 9, \b
	SUBLEQ 9, 9
.endm

.macro _MOV a=9. b=9
	SUBLEQ \b, \b
	SUBLEQ \a, 9
	SUBLEQ 9, \b
	SUBLEQ 9, 9
.endm

# registers
.set Z, 9
.set O, 10
.set A, 11
.set B, 12
.set C, 13
.set D, 14
.set N2, 15

tbl:

	SUBLEQ Z, Z, (t_main-tbl)/8

	# registers:
	t_chk: .fill 6, 8, 0
	# Z @09,   O @10,    A @11
	.quad 0; .quad 1; .quad 0
	# B @12,   C @13,    D @14
	.quad 0; .quad 0; .quad 82
	# N2 @ 15
	.quad -2

t_main:

	SUBLEQ A, A
	_MOV O, B
	_MOV B, C
	# ramp up fib until big enough
	t_fibinit:
		_ADD A, C
		_MOV B, A
		_MOV C, B
		SUBLEQ O, D, (t_fibready-tbl)/8
		SUBLEQ Z, Z, (t_fibinit-tbl)/8
	t_fibready:

	# subtract A from the input
	SUBLEQ A, (t_chk-tbl)/8

	# continue encrypting input by subtracting keys
	_ADD A, C
	_MOV B, A
	_MOV C, B
	SUBLEQ A, (t_chk-tbl)/8+1
	_ADD A, C
	_MOV B, A
	_MOV C, B
	SUBLEQ A, (t_chk-tbl)/8+2
	_ADD A, C
	_MOV B, A
	_MOV C, B
	SUBLEQ A, (t_chk-tbl)/8+3
	_ADD A, C
	_MOV B, A
	_MOV C, B
	SUBLEQ A, (t_chk-tbl)/8+4
	SUBLEQ B, (t_chk-tbl)/8+5

	# subtract encrypted input from ciphertext (want result to be -1)
	SUBLEQ (t_enc-tbl)/8, (t_chk-tbl)/8
	SUBLEQ (t_enc-tbl)/8+1, (t_chk-tbl)/8+1
	SUBLEQ (t_enc-tbl)/8+2, (t_chk-tbl)/8+2
	SUBLEQ (t_enc-tbl)/8+3, (t_chk-tbl)/8+3
	SUBLEQ (t_enc-tbl)/8+4, (t_chk-tbl)/8+4
	SUBLEQ (t_enc-tbl)/8+5, (t_chk-tbl)/8+5

	# win if t_chk is all 0xFF
	SUBLEQ N2, (t_chk-tbl)/8+2, (t_lose-tbl)/8
	SUBLEQ N2, (t_chk-tbl)/8+4, (t_lose-tbl)/8+3
	SUBLEQ N2, (t_chk-tbl)/8+1, (t_lose-tbl)/8+6
	SUBLEQ N2, (t_chk-tbl)/8+3, (t_lose-tbl)/8+9
	SUBLEQ N2, (t_chk-tbl)/8+5, (t_lose-tbl)/8+12
	SUBLEQ N2, (t_chk-tbl)/8, (t_lose-tbl)/8+15
	t_win:
	SUBLEQ 0, 0, 1	# ret 0

	t_enc:	# hidden ciphertext ;)
	.ascii "\x20\x95\xBE\xB0\x30\x94\x89\x73"
	.ascii "\xCD\xD4\x29\xEE\x47\xF6\xD2\x5D"
	.ascii "\x7A\x0A\x8E\x3F\xF6\x3E\x29\x72"
	.ascii "\xD1\x7E\x46\xC0\x8C\xBF\xD8\x71"
	.ascii "\xDA\x17\x58\x89\x02\x89\x9D\x5F"
	.ascii "\x53\xE7\x29\xCE\x96\xFE\xC3\x73"
	t_lose:	# fill with nops to foil instruction count attacks
	SUBLEQ Z, Z
	SUBLEQ Z, Z
	SUBLEQ Z, Z
	SUBLEQ Z, Z
	SUBLEQ Z, Z
	SUBLEQ Z, Z, 1	# ret

